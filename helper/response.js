exports.success = function(result, message){
    return {
        status: true,
        messages: message,
        results: result
    }
}
exports.fail = function(result, message){
    return {
        status: false,
        messages: message,
        results: result
    }
}



//gak digunakan karenya banyak kelemahan. status yg ditulis dg status di postman berbeda
// exports.ok = function(values, res){
//     let data = {
//         'status': 200,
//         'value': values
//     }
//     res.json(data)
//     res.end()
// }
// exports.fail = function(values, res){
//     let data = {
//         'status': 400,
//         'value': values
//     }
//     res.json(data)
//     res.end()
// }
