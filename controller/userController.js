var connection = require('../connection/conn')
var {success, fail} = require ('../helper/response')

exports.show = function(req, res){
    connection.query('SELECT * FROM person', function(err, result){
        if (err) return res.status(400).json(fail(err, 'fail show data'))
        res.status(200).json(success(result, "Your data"))
    })
}

exports.index = function(req, res){
    res.status(200).json(success("Hello Node JS MySQL"))
}

exports.findUserById = function(req, res){
    connection.query('SELECT * FROM person where id = ?', [req.params.user_id], function(err, result){
        if (err) return res.status(404).json(fail(err, "ID not found")) //id sembarangan ternyata tidak error, tapi hasilnya []
        res.status(200).json(success(result, "show user"))
    })
}
exports.createUser = function(req, res){
    let firstName = req.body.first_name
    let lastName = req.body.last_name
    connection.query('INSERT INTO person (first_name, last_name) VALUES (?,?)', [firstName, lastName], function(err, result){
        if (err) return res.status(422).json(fail(err.sqlMessage, "Please fill correctly"))
        res.status(201).json(success(result, "Insert data success"))
    })
}

exports.updateUser = function(req, res){
    let userId = req.params.user_id
    let firstName = req.body.first_name
    let lastName = req.body.last_name
    connection.query('UPDATE person SET first_name = ?, last_name = ? WHERE id = ?', [firstName, lastName, userId], function(err, result){
        if(err) return res.status(400).json(fail(err.sqlMessage, "Fill correctly"))
        res.status(200).json(success(result, "Update data success")) //update. semua kolom yg requirenya wajib, harus diisi. jika tidak diisi gak bisa.
    })
}

exports.deleteUserById = function(req, res){
    let userId = req.params.user_id
    connection.query('DELETE FROM person where id = ?', [userId], function(err, result){
        if(err) return res.status(400).json(fail(err.sqlMessage, "Fill correctly"))
        res.status(200).json(success(result, "Delete data success"))
    })
}
