var express = require('express')
var app = express()
var env = require('dotenv').config();
var port = process.env.PORT
var bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

var morgan = require('morgan')
app.use(morgan('dev'))

var routes = require('./routes/userRouter')
routes(app)

app.listen(port)
console.log(`server started on port: ${port}`)
