module.exports = function(app){
    var user = require('../controller/userController')

    app.route('/').get(user.index)

    app.route('/users').get(user.show)

    app.route('/users/:user_id').get(user.findUserById)

    app.route('/users').post(user.createUser)

    app.route('/users/:user_id').put(user.updateUser)

    app.route('/users/:user_id').delete(user.deleteUserById)
}
